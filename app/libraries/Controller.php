<?php

/*
 * Base Controller
 * Loads the model and the views
 */

class Controller
{
    // Load model
    public function model($model){
        // Require model
        if(file_exists('../app/models/' . $model . '.php')){
            require_once '../app/models/' . $model . '.php';
            // Instantiate model
            return new $model();
        } else {
            die('Model does not exist.');
        }
    }

    // Load view
    public function view($view, $data = []){
        // Check for the view file
         if(file_exists('../app/views/' . $view . '.php')){
             require_once '../app/views/' . $view . '.php';
         } else {
             die('View does not exist.');
         }
    }
}
