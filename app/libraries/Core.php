<?php

/*
 * App Core Class
 * Create URL & loads core controller
 * URL FORMAT - /controller/method/params
 */

class Core
{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {

        $url = $this->getUrl();

        // Look in controllers for first value of the array $url = explode('/', $url) in getURL()
        if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {

            // If controller file exists, set as currentController
            $this->currentController = ucwords($url[0]);

            // Unset Index 0
            unset($url[0]);
        }

        // Require the controller
        require_once '../app/controllers/' . $this->currentController . '.php';

        // Instantiate controller class
        $this->currentController = new $this->currentController;

        // Check for the second part of the url
        if (isset($url[1])) {
            // Check to see if the method exists in the controller
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
                // Unset Index 2
                unset($url[1]);
            }
        }

        // Get paramaters
        $this->params = $url ? array_values($url) : [];

        // Call a callback with arrays of params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);


    }

    public function getUrl()
    {
        if (isset($_GET['url'])) {
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }
}
