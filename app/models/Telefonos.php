<?php
class Telefonos 
{
    private $db;

    public function __construct()
    {
     $this->db = new Database;
    }
    
    public function Index()
    {
       $this->db->query('SELECT * FROM telefonos');
      return $results = $this->db->resultSet();
    }
    public function store($data)
    {
      $this->db->query('INSERT INTO telefonos (nombre, precio) VALUES(:nombre, :precio)');
      // Bind values
      $this->db->bind(':nombre', $data['nombre']);
      $this->db->bind(':precio', $data['precio']);
  
      // Execute
      if($this->db->execute()){
          return true;
      } else {
          return false;
      }
    }
    public function delete($id)
    {
      $this->db->query('DELETE FROM telefonos where id = :id');
      // Bind values
      $this->db->bind(':id', $id);
  
      // Execute
      if($this->db->execute()){
          return true;
      } else {
          return false;
      }
    }
}
?>