<?php
class Autos 
{
    private $db;

    public function __construct()
    {
            $this->db = new Database;
    }
    
    public function Index()
    {
        $this->db->query('SELECT * FROM autos');
      //  $results = $this->db->fetch_assoc();
      return $results = $this->db->resultSet();

        //return $results;
    }
    public function store($data)
    {
        $this->db->query('INSERT INTO autos (nombre, descripcion, precio) VALUES(:nombre, :descripcion, :precio)');
        // Bind values
        $this->db->bind(':nombre', $data['nombre']);
        $this->db->bind(':descripcion', $data['descripcion']);
        $this->db->bind(':precio', $data['precio']);
    
        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }
    public function show($id)
    {
        $this->db->query('SELECT * FROM autos WHERE id = :id');
        $this->db->bind(':id', $id);
        $row = $this->db->single();
        return $row;
    }
    public function update($data)
    {
        $this->db->query('UPDATE autos SET nombre = :nombre, descripcion = :descripcion, precio = :precio WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':nombre', $data['nombre']);
        $this->db->bind(':descripcion', $data['descripcion']);
        $this->db->bind(':precio', $data['precio']);

        // Execute
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }
    public function delete($id)
    {
        $this->db->query('DELETE FROM autos WHERE id = :id');
        $this->db->bind(':id', $id);
        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }
}
?>