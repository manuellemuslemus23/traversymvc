<?php

// DB params
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', 'manuel');
define('DB_NAME', 'sharepost');

// App root
define('APP_ROOT' , dirname(dirname(__FILE__)));

// URL root
define('URL_ROOT', 'http://localhost/traversymvc-3');

// Site name 
define('SITE_NAME', '_YOUR_SITE_NAME_');