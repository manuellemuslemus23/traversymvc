<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    </head>
    <body>
    <div class="container">
        <input type="text">
        <div class="">
            <table>
                <thead>
                    <th>Nombre</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Editar</th>
                    <th>Eliminar</th>
                </thead>
                <tbody>
                <?php foreach($data['autos'] as $auto) :?>
                <tr>
                    <td><?php echo $auto->nombre; ?></td>
                    <td><?php echo $auto->descripcion; ?></td>
                    <td>$ <?php echo $auto->precio; ?></td>
                    <td> 
                    <form action="<?php echo URL_ROOT; ?>/pages/edit" method="post">
                    <input type="hidden" name="id" value="<?php echo $auto->id; ?>"><br><br>
                    <input type="submit" class="btn" value="Editar">               
                    </form>
                    </td>
                    <td>
                    <form action="<?php echo URL_ROOT; ?>/pages/delete" method="post">
                    <input type="hidden" name="id" value="<?php echo $auto->id; ?>"><br><br>
                    <input type="submit" class="btn red" value="Eliminar">               
                    </form>                    </td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
     
    </div>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        
    </body>
</html>