<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    </head>
    <body>
    <div class="container">
    <br>
    <br>
    <br>
   
    <h4>Editando:</h4>
        <?php foreach($data as $auto) :?>
            <form action="<?php echo URL_ROOT; ?>/pages/update" method="post">
                <label for="">Nombre :</label>
                <input type="text" name="pnombre" value="<?php echo $auto->nombre; ?>"><br><br>
                <label for="">Descripcion :</label>
                <input type="text" name="pdescripcion" value="<?php echo $auto->descripcion; ?>"><br><br>
                <label for="">Precio :</label>
                <input type="text" name="pprecio" value="<?php echo $auto->precio; ?>"><br><br>
                <input type="hidden" name="pid" value="<?php echo $auto->id; ?>"><br><br>

                <input type="submit" class="btn teal" value="Guardar">
            </form>
        <?php endforeach; ?>
    </div>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        
    </body>
</html>