<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> 
</head>
<body>
   <div class="container">
   <h3>Lista de Telefonos</h3> 
        <?php foreach($data['telefonos'] as $tel): ?>
        <div clas="row">
        <h4 clas="col s2"><?php echo $tel->nombre; ?></h4>
        <h4 clas="col s2"><?php echo $tel->precio; ?></h4>
        <form action="<?php echo URL_ROOT; ?>/Telefono/delete" method="post">
            <input type="hidden" name="id" value="<?php echo $tel->id; ?>">
            <input type="submit" class="btn red" value="Eliminar">
        </form>
        <hr>
        </div>
        <?php endforeach; ?>
   </div>
   <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>