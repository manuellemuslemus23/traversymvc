<?php

class Telefono extends Controller
{
    public function __construct(){
        $this->telefonosModel = $this->model('Telefonos');
    }

    public function Index()
    {
        $telefonos = $this->telefonosModel->Index();
        $data = [
            'telefonos' => $telefonos
        ];
        $this->view('telefono/index', $data);
    }
    public function create()
    {
        $this->view('telefono/create');
    }
    public function store()
    {
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
            $data = [
                'nombre' => trim($_POST['pnombre']),
                'precio'  => trim($_POST['pprecio'])
            ];
            $telefonos = $this->telefonosModel->store($data);
            if($telefonos)
            {
                //$this->view('pages/create');
                header('Location: http://localhost/traversymvc-3/Telefono');
                //echo "echo papus";
            }
            else{
                echo "algo salio mal";
            }
        }else
        {
            header('Location: http://localhost/traversymvc-3/Telefono/create');
        }    
    }
    public function delete()
    {
        $id = $_POST['id'];
        $telefonos = $this->telefonosModel->delete($id);
        if($telefonos)
        {
        header('Location: http://localhost/traversymvc-3/Telefono');
        }
        else{
            echo "ups algo a salido mal";
        }
    }

}
?>