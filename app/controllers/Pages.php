<?php

class Pages extends Controller
{
    public function __construct(){
        $this->postModel = $this->model('Autos');
    }

    public function index()
    {
        $autos = $this->postModel->Index();
        $data = [
            'autos' => $autos
        ];
        $this->view('pages/hola', $data);
    }

    public function create()
    {
        $this->view('pages/create');
    }
    public function store()
    {        
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
            $data = [
                'nombre' => trim($_POST['pnombre']),
                'descripcion' => trim($_POST['pdescripcion']),
                'precio'  => trim($_POST['pprecio'])
            ];
            $autos = $this->postModel->store($data);
            if($autos)
            {
                //$this->view('pages/create');
                header('Location: http://localhost/traversymvc-3/');
                //echo "echo papus";
            }
            else{
                echo "algo salio mal";
            }
        }else
        {
            header('Location: http://localhost/traversymvc-3/pages/create');
        }    
    }

    public function edit()
    {   
        $id = $_POST['id'];
        $data = $this->postModel->show($id);

        $this->view('pages/edit', compact('data'));
    }

    public function update()
    {   
        $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $data = [
                'id' => trim($_POST['pid']),
                'nombre' => trim($_POST['pnombre']),
                'descripcion' => trim($_POST['pdescripcion']),
                'precio'  => trim($_POST['pprecio'])
            ];
            $autos = $this->postModel->update($data);
            if($autos)
            {
                //$this->view('pages/create');
                header('Location: http://localhost/traversymvc-3/');
                //echo "echo papus";
            }
            else{
                echo "algo salio mal";
            }
        }else
        {
            header('Location: http://localhost/traversymvc-3/pages/create');
        }    
            
    }

    public function delete()
    {   
        $id = $_POST['id'];
        $auto = $this->postModel->delete($id);
        if($auto)
        {
            //$this->view('pages/create');
            header('Location: http://localhost/traversymvc-3/');
            //echo "echo papus";
        }
        else{
            echo "algo salio mal";
        }
    }
    public function about(){
        $data = [
            'title' => 'About us'
        ];
        $this->view('pages/about', $data);
    }
}
?>